// alert('test')

/*  Exponents
    -we use ** for exponent operator
 */

const firstNum = Math.pow(8,2)
console.log(firstNum)

const secondNum = 8 ** 2;
console.log(secondNum)

const thirdNum = 5 ** 5
console.log(thirdNum)

// Template Literals
/*  allows us to write string without using the concatenation operator (+)
 */

let name = "George";

// Concatenation / Pre-Template Literals
// Using single quote ('')

let message = 'Hello ' + name + ', Welcome to Zuitt Coding Bootcamp';
console.log('Message without template literals' + message)
console.log(message)

console.log('')

// String using Template Literals
// Uses the backticks(``)

message = `Hello ${name}, Welcome to Zuitt Coding Bootcamp`;
console.log(`Message with template literals : ${message}`);

let anotherMessage = `
${name} attended a math competition.
He won by solving the problem 8 ** 2 with solution of ${secondNum}
`;

console.log(anotherMessage)

anotherMessage = "\ " + name + " attended a math competition. \n He won it by solving the problem 8 ** 2 with the solution " + secondNum + ". \n";

console.log(anotherMessage)



// Operation inside template literals

const interestRate = 0.1;
const principal = 1000;

console.log(`The interest on your saving is : ${principal * interestRate}`)

//Array Desctructuring
/*  
    Allows to unpack elements in an array into distinct variables.
    Allows us to name the array elements with variables instead of index number
    Syntax:
        let/const [variableName, variableName, variableName] = array;
*/

const fullName = ['Joe', 'Dela', 'Cruz' ];
console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])

console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you. `)


const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);


console.log(`Hello, ${firstName} ${middleName} ${lastName}. It's nice to meet you. `)


// Object Destructuring
/* 
    Allows to unpack properties of objects into distinct variables. Shortens the syntax for accessing properties from objects

    Syntax:
        let const {propertyName, propertyName, propertyName} = object
*/

const person = {
    givenName: 'Jane',
    maidenName: 'Dela',
    familyName: 'Cruz'
};

// Pre-object destructuring
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

// function getFullName(firstName, middleName, familyName){
//     console.log(`${firstName} ${middleName} ${familyName}`);
// }

// getFullName(person.firstName, person.middleName, person.familyName);

// Using Object Destructuring
const {maidenName, givenName, familyName} = person;

function getFullName(givenName, maidenName, famName){
    console.log(`${givenName} ${maidenName} ${famName}`);
}

getFullName(givenName, maidenName, familyName);


// Mini Activity:

	/*Item 1.)
		- Create a variable employees with a value of an array containing names of employees
		- Destructure the array and print out a message with the names of employees using Template Literals.*/


	// Code here:

    const employees = ["Amie Lim","Rica Dee","Raymond Rivera"];
    const [amie, rica, raymond] = employees;

    console.log(`Employees are : ${amie}, ${rica} and ${raymond}`)


 /* 

	/*Item 2.)
		- Create a variable pet with a value of an object data type with different details of your pet as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	// Code here:

    const pet = {
        petName : "Mingming",
        petType : "cat",
        petColor : "orange"
    }

    const {petName, petType, petColor} = pet;

    console.log(`I have an ${petColor} pet ${petType} named ${petName}`)

    // PRE-ARROW FUNCTION AND ARROW FUNCTION
    /* 
        PRe-arrow Function
            Syntax:
                function functionName(paramA, paramB){
                    statement //console.log //return
                }
    
    */


function printFullName(firstName, middleInitial, lastName){

    console.log(firstName, middleInitial, lastName)
}

printFullName('Rupert','B','Ramos')

// Arrow Function
/* 
    Syntax:
        let/const variableName = (paramA, paramB) => {
            statement // console.log //return
        }

*/

const printAnotherName = (firstName, middleInitial, lastName) => {
    console.log(`${firstName} ${middleInitial} ${lastName}`)
}

printAnotherName('John','A','Vega')

const students = ['Yoby','Emman','Ronel']

/* 
    Functions with Loop
    Pre-Arrow

*/

students.forEach(function(student){
    console.log(student + " is a student")
})

/* 
    Arrow Function

*/
students.forEach((student) => {
    console.log(`${student} is a student`)
})

// Implicit return statement

// Pre-Arrow
function add(x, y){
    return x + y;
}

let total = add(4, 5)
console.log(total)

// Arrow
const addition = (x, y) => x + y;
let total2 = addition(12,15);

console.log(total2)


// use return keyword when curly braces is present


// Default function argument

const greet = (name = "User") => {
    return `Good evening, ${name}`
}

console.log(greet())
console.log(greet('Archie'))

// Class-based Object Blueprint
/* 
    Allows creation/instantiation of objects using class as blueprint
    Syntax:
        class ClassName {
            constructor(objectPropertyA, objectPropertyB){
                this.objectPropertyA = objectPropertyA;
                this.objectPropertyB = objectPropertyB;
            }
        }
*/

class Car {

    constructor(brand, name, year){
        this.brand = brand
        this.name = name
        this.year = year
    }
}

const myCar = new Car();
console.log(myCar)

myCar.brand = "Ford";
myCar.name = "Ranger Raptor"
myCar.year = 2021;

console.log(myCar)

const myNewCar = new Car("Toyota","Vios", 2019)
console.log(myNewCar)


    
